

# Tries to guess your birth month and year with a prompt formatted like Guess «guess number» : «name»
# were you born in «m» / «yyyy» ? then prompts with "yes or no?"
# If the computer guesses it correctly because you respond yes,
# it prints the message I knew it! and stops guessing
# If the computer guesses incorrectly because you respond no,
# it prints the message Drat! Lemme try again! if it's only guessed 1, 2, 3, or 4 times. Otherwise, it prints the message I have other things to do. Good bye.

from random import randint
name = input("Hi! What is your name? ")

for num in range(1,6):
    random_month = randint(1,12)
    random_year = randint(1924,2004)

    print(f'Guess {num} : {name} were you born in {random_month} / {random_year} ?')
    answer = input("yes or no? ").lower()

    if answer == "yes":
        print("I knew it!")
        exit()
    elif num == 5:
        print("I have another thing to do.Goodbye")
    else:
        print("Drat! Lemme try again!")
